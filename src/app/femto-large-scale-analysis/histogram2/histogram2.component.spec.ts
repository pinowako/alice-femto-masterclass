import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Histogram2Component } from './histogram2.component';

describe('Histogram2Component', () => {
  let component: Histogram2Component;
  let fixture: ComponentFixture<Histogram2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Histogram2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Histogram2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
