import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistogramDividedComponent } from './histogram-divided.component';

describe('HistogramDividedComponent', () => {
  let component: HistogramDividedComponent;
  let fixture: ComponentFixture<HistogramDividedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistogramDividedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistogramDividedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
