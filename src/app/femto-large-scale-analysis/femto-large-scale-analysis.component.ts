import { Component, OnInit, ViewChild } from '@angular/core';
import { hsl } from 'd3';
import { HistogramDividedComponent } from './histogram-divided/histogram-divided.component';
import { Options } from "@angular-slider/ngx-slider";


@Component({
  selector: 'app-femto-large-scale-analysis',
  templateUrl: './femto-large-scale-analysis.component.html',
  styleUrls: ['./femto-large-scale-analysis.component.css'],
})
export class FemtoLargeScaleAnalysisComponent implements OnInit {

  public twoHistShow = false;

  public value: number = 0.04;
  public highValue: number = 0.08;
  public options: Options = {
    floor: 0,
    ceil: 0.12,
    step: 0.001
  }
  
  constructor() {}


  ngOnInit(): void {

  }

}
