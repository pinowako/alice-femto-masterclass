import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FemtoLargeScaleAnalysisComponent } from './femto-large-scale-analysis/femto-large-scale-analysis.component';
import { HistogramComponent } from './femto-large-scale-analysis/histogram/histogram.component';
import { Histogram2Component } from './femto-large-scale-analysis/histogram2/histogram2.component';
import { HistogramDividedComponent } from './femto-large-scale-analysis/histogram-divided/histogram-divided.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule} from '@angular/material/card';  
import { MatButtonModule} from '@angular/material/button';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { InstructionsComponent } from './instructions/instructions.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';



@NgModule({
  declarations: [
    AppComponent,
    FemtoLargeScaleAnalysisComponent,
    HistogramComponent,
    Histogram2Component,
    HistogramDividedComponent,
    InstructionsComponent,
    AboutComponent,
    ContactComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    NgxSliderModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
