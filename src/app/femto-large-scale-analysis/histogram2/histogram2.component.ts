import { Output, EventEmitter } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-histogram2',
  templateUrl: './histogram2.component.html',
  styleUrls: ['./histogram2.component.css']
})
export class Histogram2Component implements OnInit {
  private svg:any;
  private margin = 60;
  private width = 600 - (this.margin * 2);
  private height = 400 - (this.margin * 2);


  private createSvg(): void {
    this.svg = d3.select("figure#bar-num")
    .append("svg")
    .attr("width", this.width + (this.margin * 2))
    .attr("height", this.height + (this.margin * 2))
    .append("g")
    .attr("transform", "translate(" + this.margin + "," + this.margin + ")")
    ;
    
    
  }

  private drawBars(data: any[]): void {
    // Create the X-axis band 

    const x = d3.scaleBand()
    .range([0, this.width])
    .domain(data.map(function(d) { return d.X; }))
    .padding(0);

    // Draw the X-axis on the DOM
    this.svg.append("g")
    .attr("transform", "translate(0," + this.height + ")")
    .call(d3.axisBottom(x))
    .selectAll("text")
    .attr("transform", "translate(-10,0)rotate(-45)")
    .style("text-anchor", "end")
    

    d3.select("#bar-num").selectAll(".tick").each(function(d, i) {
      if (i % 10 != 0) {
        d3.select(this).remove()
      }
    })

    this.svg.append("text")
    .attr("class", "x label")
    .attr("text-anchor", "end")
    .attr("x", this.width)
    .attr("y", this.height + 50)
    .text("q_{inv}  [GeV/c]");

    let yy = data.map(function(d:any) { return d.Y; })

    let max_y = Math.max(...yy)
 
    // Create the Y-axis band scale
    const y = d3.scaleLinear()
    //.domain([0, d3.max(data, function(d) { return d.Y; })])
    .domain([0, max_y + max_y*0.1])
    .range([this.height, 0]);

    // Draw the Y-axis on the DOM
    this.svg.append("g")
    .call(d3.axisLeft(y).tickFormat(function(d){
      return "" + d;
  }).ticks(9));

    // Create and fill the bars
    this.svg.selectAll("bars")
    .data(data)
    .enter()
    .append("rect")
    .attr("x", (d:any) => x(d.X))
    .attr("y", (d:any) => y(d.Y))
    .attr("width", x.bandwidth())
    .attr("height", (d:any) => this.height - y(d.Y))
    .attr("fill", "#d04a35");


    this.svg.append("text")
        .attr("x", (this.width / 2))             
        .attr("y", 0 - (this.margin / 2))
        .attr("text-anchor", "middle")  
        .style("font-size", "25px") 
        .style("text-decoration", "underline")  
        .text("Numerator");



  } 

  public drawNumHist(num:any){
    let N = (num.target as HTMLSelectElement).value;
    if (parseInt(N) == 0){
      this.svg.selectAll("*").remove(); //delete anything first
      d3.csv("/assets/num_cent_0.csv").then(data => this.drawBars(data)); 
    }
    if (parseInt(N) == 1){
      this.svg.selectAll("*").remove(); //delete anything first
      d3.csv("/assets/num_cent_1.csv").then(data => this.drawBars(data)); 
    }
    if (parseInt(N) == 2){
      this.svg.selectAll("*").remove(); //delete anything first
      d3.csv("/assets/num_cent_2.csv").then(data => this.drawBars(data)); 
    }
    if (parseInt(N) == 3){
      this.svg.selectAll("*").remove(); //delete anything first
      d3.csv("/assets/num_cent_3.csv").then(data => this.drawBars(data)); 
    }
    if (parseInt(N) == 4){
      this.svg.selectAll("*").remove(); //delete anything first
      d3.csv("/assets/num_cent_4.csv").then(data => this.drawBars(data)); 
    }
    if (parseInt(N) == 5){
      this.svg.selectAll("*").remove(); //delete anything first
      d3.csv("/assets/num_cent_5.csv").then(data => this.drawBars(data)); 
    }
    if (parseInt(N) == 6){
      this.svg.selectAll("*").remove(); //delete anything first
      d3.csv("/assets/num_cent_6.csv").then(data => this.drawBars(data)); 
    }
  }

  constructor() { }

  ngOnInit(): void {

    this.createSvg();

  }

}
