import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FemtoLargeScaleAnalysisComponent } from './femto-large-scale-analysis.component';

describe('FemtoLargeScaleAnalysisComponent', () => {
  let component: FemtoLargeScaleAnalysisComponent;
  let fixture: ComponentFixture<FemtoLargeScaleAnalysisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FemtoLargeScaleAnalysisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FemtoLargeScaleAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
