import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HistogramDividedComponent } from './femto-large-scale-analysis/histogram-divided/histogram-divided.component';
import { FemtoLargeScaleAnalysisComponent } from './femto-large-scale-analysis/femto-large-scale-analysis.component';
import { InstructionsComponent } from './instructions/instructions.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';


const routes: Routes = [
  { path: '', component: InstructionsComponent},
  { path: 'femto-large-scale-analysis', component: FemtoLargeScaleAnalysisComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
