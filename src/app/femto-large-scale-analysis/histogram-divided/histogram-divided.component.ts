import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NG_ASYNC_VALIDATORS } from '@angular/forms';
import * as d3 from 'd3';
import * as fmin from '@nwaltham/fmin';
import { BehaviorSubject, forkJoin, from, range } from 'rxjs';
import {Router, NavigationEnd,ActivatedRoute} from '@angular/router'
import { CompileNgModuleMetadata, flatten, ThisReceiver } from '@angular/compiler';
import { keyframes } from '@angular/animations';
import { geoAlbers, min, thresholdSturges } from 'd3';
import { fakeAsync } from '@angular/core/testing';
import { versions } from 'process';

@Component({
  selector: 'app-histogram-divided',
  templateUrl: './histogram-divided.component.html',
  styleUrls: ['./histogram-divided.component.css']
})
export class HistogramDividedComponent implements OnInit {


  private norm = false;
  private addFit = false;
  public errNum:any[]=[]
  public errDen:any[]=[]
  public errDiv:any[]=[]
  public showTwo:any = false;
  public showScatter:any = false;
  public showDiv:any = false;

  private svg:any;
  private svg2:any;
  private margin = 100;
  private width = 850 - (this.margin * 2);
  private height =600 - (this.margin * 2);
  

  public range_min:any;
  public range_max:any;
  private R_choice:any = 5; // no reason, just a placeholder!
  private params:any;
  private fit_data:any;
  public chi_squared:any;
  public lambda:any;
  public R:any;
  public showParams = false;
  public displayed_R:any; //R which I display to the user (different by a hbar*c factor)

  private data1:any;
  private data2:any;
  private r2_data:any;
  private r3_data:any;
  private r4_data:any;
  private r5_data:any;
  private r6_data:any;
  private r7_data:any;
  private r8_data:any;
  private r9_data:any;
  private r10_data:any;
  public cent:any; //centrality  

  //to disable buttons!!
  public enableNorm = false;
  public enableR = false;
  public enableDiv = false;

  public remv_cent5:any; // just to cut off 1st pioint in cent5
  public remv_cent2:any;
  //So in all Den and Num files, the x values are the same. Therefore I wrote a code in python which 
  //interpolates the K(q) function and chooses the exact x values I need for the bins to match
  //so for this code to work, x values which are inputed here must be the same! No internal way to solve that in this code!


  public displayHist(){
    //Had to be loaded this way
    this.errDen = []
    this.errNum = []
    this.errDiv = []
    this.svg.selectAll("*").remove();

    forkJoin([from(d3.csv("/assets/num_cent_0.csv")), from(d3.csv("/assets/den_cent_0.csv")),from(d3.csv("/assets/num_cent_1.csv")), from(d3.csv("/assets/den_cent_1.csv")),from(d3.csv("/assets/num_cent_2.csv")), from(d3.csv("/assets/den_cent_2.csv")), from(d3.csv("/assets/num_cent_3.csv")), from(d3.csv("/assets/den_cent_3.csv")),from(d3.csv("/assets/num_cent_4.csv")), from(d3.csv("/assets/den_cent_4.csv")),from(d3.csv("/assets/num_cent_5.csv")), from(d3.csv("/assets/den_cent_5.csv")), from(d3.csv("/assets/num_cent_6.csv")), from(d3.csv("/assets/den_cent_6.csv")),from(d3.csv("/assets/r2_k_data.csv")),  from(d3.csv("/assets/r3_k_data.csv")),  from(d3.csv("/assets/r4_k_data.csv")),  from(d3.csv("/assets/r5_k_data.csv")), from(d3.csv("/assets/r6_k_data.csv")), from(d3.csv("/assets/r7_k_data.csv")), from(d3.csv("/assets/r8_k_data.csv")),  from(d3.csv("/assets/r9_k_data.csv")), from(d3.csv("/assets/r10_k_data.csv"))]).subscribe(res => {
      
      //A very hard coded way to select the divided histogram data
      if(this.cent ==0){
        this.data1 = res[0]
        this.data2 = res[1]
      }
      if(this.cent == 1){
        this.data1 = res[2]
        this.data2 = res[3]
      }
      if(this.cent == 2){
        this.data1 = res[4]
        this.data2 = res[5]
        this.remv_cent2 = 1
        
      }
      if(this.cent == 3){
        this.data1 = res[6]
        this.data2 = res[7]
      }
      if(this.cent == 4){
        this.data1 = res[8]
        this.data2 = res[9]
      }
      if(this.cent == 5){
        this.data1 = res[10]
        this.data2 = res[11]
        this.remv_cent5 = 1
      }
      if(this.cent == 6){
        this.data1 = res[12]
        this.data2 = res[13]
      }


      //clean the data to be how I want it
      let d_joined:any = this.cleanData(this.data1, this.data2)  

      //Check which fit data to add
      if (this.R_choice == 2){
        this.r2_data = res[14] //load
        //find the fit data
        this.params = this.fit(d_joined, this.r2_data);
        //Fit using those parameters
        this.fit_data = this.calcModel(d_joined, this.params, this.r2_data)
        //////
      }else if(this.R_choice == 3){
        this.r3_data = res[15]
        this.params = this.fit(d_joined, this.r3_data);
        this.fit_data = this.calcModel(d_joined, this.params, this.r3_data)
      }else if(this.R_choice == 4){
        this.r4_data = res[16]
        this.params = this.fit(d_joined, this.r4_data);
        this.fit_data = this.calcModel(d_joined, this.params, this.r4_data)
      }else if(this.R_choice == 5){
        this.r5_data = res[17]
        this.params = this.fit(d_joined, this.r5_data);
        this.fit_data = this.calcModel(d_joined, this.params, this.r5_data)
      }else if(this.R_choice == 6){
        this.r6_data = res[18]
        this.params = this.fit(d_joined, this.r6_data);
        this.fit_data = this.calcModel(d_joined, this.params, this.r6_data)
      }else if(this.R_choice == 7){
        this.r7_data = res[19]
        this.params = this.fit(d_joined, this.r7_data);
        this.fit_data = this.calcModel(d_joined, this.params, this.r7_data)
      }else if(this.R_choice == 8){
        this.r8_data = res[20]
        this.params = this.fit(d_joined, this.r8_data);
        this.fit_data = this.calcModel(d_joined, this.params, this.r8_data)
      }else if(this.R_choice == 9){
        this.r9_data = res[21]
        this.params = this.fit(d_joined, this.r9_data);
        this.fit_data = this.calcModel(d_joined, this.params, this.r9_data)
      }else if(this.R_choice == 10){
        this.r10_data = res[22]
        this.params = this.fit(d_joined, this.r10_data);
        this.fit_data = this.calcModel(d_joined, this.params, this.r10_data)
      }
      

      

      //Draws histogram bars
      this.drawBars(d_joined, this.fit_data);

    })

  }



  private createSvg(): void {
    this.svg = d3.select("figure#hist-div")
    .append("svg")
    .attr("width", this.width + (this.margin * 2))
    .attr("height", this.height + (this.margin * 2))
    .append("g")
    .attr("transform", "translate(" + this.margin + "," + this.margin + ")");
  }


  //data = num    data2 = den
  ///Takes the input data and makes it into what I want for div hist + normalize
  private cleanData(data: any[], data2: any[]){
    //load data works!

    const x1 = data.map(function(d) { return d.X; })
    const x2 = data2.map(function(i) { return i.X; })

    const y1 = data.map(function(d) { return d.Y; })
    const y2 = data2.map(function(i) { return i.Y; })

    let y_new = [];

    this.errDiv.splice(0,this.errDiv.length)

    let m = 0
    //remove zeros to avoid NaN later after division, NOTE ALL 0's are in the first few bins!! Will
    while (m < y2.length){
      //console.log(y2[i])
      if (y2[m] == 0 || y1[m] == 0) {
        x1.splice(m, 1)
        y1.splice(m, 1)
        y2.splice(m, 1)
      }else{
        ++m
      }
    }

    //just to remove the first point with cent 5 or cent2!! Since they are wierd and huge errorbars
    if (this.remv_cent5 == 1 || this.remv_cent2 == 1){
      x1.splice(0, 1)
      y1.splice(0, 1)
      y2.splice(0, 1)
    }


    ///FIND ERRORS IN NUM AND DEN
    for (let k = 0; k<y1.length; k++){
      this.errDen.push(Math.sqrt(y2[k]))
      this.errNum.push(Math.sqrt(y1[k]))
    }

    const x_new = []  //x-new is just scaled by 1/2
    //division works!
    for (let i = 0; i < y2.length; i++) {
      y_new.push(parseFloat(y1[i]) / parseFloat(y2[i]))
      x_new.push((x1[i]/2).toString())   //Scale x by 2 and make them str because of .domain of x
    }
 
    ////NOW FIND ERROR IN DIV HIST USING ERROR PROPAGATION
    let err = 0;
    for (let k = 0; k<y_new.length; k++){
      err = y_new[k]*Math.sqrt( (this.errNum[k]/y1[k])*(this.errNum[k]/y1[k])  + (this.errDen[k]/y2[k])*(this.errDen[k]/y2[k])  )
      this.errDiv.push(err)
    }


/*
    //This is a way to remove points after 0,5 as idk how to zoom properly
    var g = 0
    while (g < x_new.length){
      if (parseFloat(x_new[g]) > 0.4) {   ///Unitl where do I zoom
          x_new.splice(g, 1)
          y_new.splice(g, 1)
          this.errDiv.splice(g,1)
      }else{
         ++g
        }
      }

*/
      //Remove all the 0's as it messes up the FIT function later!
      //VERY IMPORTANT 
      let c = 0
      while (c < y_new.length){
        if (y_new[c]== 0) {   ///Unitl where do I zoom
            y_new.splice(c, 1)
            x_new.splice(c, 1)
        }else{
           ++c
        }
      }


      ///Normalizing here
      let y_norm:any = []

      if (this.norm == true){
        let N_den = 0;
        let N_num = 0;
  
        for (let i=0; i<y1.length; i++) {
          if (parseFloat(x1[i])>this.range_min && parseFloat(x1[i])<this.range_max) {
            N_den = N_den + parseFloat(y2[i])
            N_num = N_num + parseFloat(y1[i])
          }
        };

        let norm_c = N_den/N_num;

        for (let n = 0; n < y2.length; n++) {
          y_norm.push(y_new[n]*norm_c)
          this.errDiv[n] = this.errDiv[n]*norm_c
        }
        //// Normalization
      }
      else{
        y_norm = y_new
      }
    
    const d_joined = [];
    for (let i = 0; i < x_new.length; i++) {
        d_joined.push({X: x_new[i], Y: y_norm[i], Err: this.errDiv[i]});
    }


    console.log(d_joined)
    return d_joined
  }


  //Draw SCATTERPLOT, NOT ACTUALLY BARS
  private drawBars(d_joined:any, fit_data:any[]): void {

    ///Call to clean the data
    let x_new = d_joined.map(function(d:any) { return d.X; })
    let y_new = d_joined.map(function(d:any) { return d.Y; })

    //Define x axis
    let max_x = Math.max(...x_new)
    const x = d3.scaleLinear()
    .domain([0, max_x + max_x*0.1]) //hardcode
    .range([ 0, this.width ]);

    ///More x axis properties
    this.svg.append("g")
    .style("font", "14px times")
    .attr("transform", "translate(0," + this.height + ")")
    .call(d3.axisBottom(x).tickFormat(function(d){
      return "" + d;
    }).ticks(10)).style("font", "14px times")

    // Add Y axis
    //Find min and max for the range
      //find maxx
    let max_y = Math.max(...y_new)
    //min ends up 0 :(, so I have to use this to find min whihc is not 0
    var min_y = Math.min.apply(null, y_new.filter((n:any) => n != 0));

  
    const y = d3.scaleLinear()
    .domain([min_y - min_y*0.2, max_y + max_y*0.1]) // a bit above max
    .range([this.height, 0]);


    this.svg.append("g")
    .call(d3.axisLeft(y).tickFormat(function(d){
      return "" + d;
    }).ticks(10)).style("font", "14px times")


    // Add dots
    const dots1 = this.svg.append('g');
    dots1.selectAll("dot")
    .data(d_joined)
    .enter()
    .append("circle")
    .attr("cx", (d:any) => x(d.X))
    .attr("cy", (d:any) => y(d.Y))
    .attr("r", 3)
    .style("opacity", .8)
    .style("fill", "blue");


    //Errorbars
    var lines = this.svg.selectAll('line.error')
    .data(d_joined);
     lines.enter()
    .append('line')
    .attr('class', 'error')
    .merge(lines)
    .attr('x1', function(d:any) { return x(d.X); })
    .attr('x2', function(d:any) { return x(d.X); })
    .attr('y1', function(d:any) { return y(d.Y + d.Err); })
    .attr('y2', function(d:any) { return y(d.Y - d.Err); })
    .attr("stroke", "black").attr("stroke-width", "1px").attr("fill", "none");
    /////

    // Add labels
    dots1.selectAll("text")
    .data(d_joined)
    .enter()
    .append("text")
    .text((d:any) => d.Framework)
    .attr("x", (d:any) => x(d.X))
    .attr("y", (d:any) => y(d.Y))

    this.svg.append("text")
    .attr("class", "x label")
    .attr("text-anchor", "end")
    .attr("x", this.width)
    .attr("y", this.height + 50)
    .text("k* [GeV/c]");

    this.svg.append("text")
    .attr("class", "y label")
    .attr("text-anchor", "end")
    .attr("y", -50)
    .attr("dy", ".75em")
    .attr("transform", "rotate(-90)")
    .text("C (k*)");


    ///Here draw fit
    if (this.addFit == true){
      this.drawFit(d_joined, x, y, fit_data)
    }
  } 



  //Calculates the y value based on x and parameters, works nicely, to be used in the fit function
  private model(a: Array<number>, x: Array<number>, k: Array<number>): Array<number> {
    const results:any = [];

    let lambda = a[0];
    let R = a[1]

    for(let i = 0; i < x.length; i++) {
          results.push((1-lambda) + lambda*k[i]*(1 + Math.exp(- x[i]*x[i] * R*R)))
      }

    return results;
  }





  //Here is where I add my initial guess 
  public aParameters: [number, number] = [0.6, this.R_choice];  // lambda and R respectivelly



  public fit(d_joined:any, r_data:any) {

    //loading
    let x = d_joined.map(function(d:any) { return d.X; })
    let y = d_joined.map(function(d:any) { return d.Y; })

    let K_dat = r_data.map(function(d:any) { return d.K; })
    let X_dat = r_data.map(function(d:any) { return d.X; })



    //I need to do something here to cut K in length and match to the closest x val!!!
    // len x is around 98 or 97 and len k is 100, so remove first 2 or 3
    //I will have to do this again in another function!! No need to generalize as I only do it twice
    let b = 0
    while (b < K_dat.length - x.length){
      if (b < K_dat.length - x.length) {   ///Unitl where do I zoom
          K_dat.splice(b, 1)
      }else{
         ++b
        }
      }


    let fit_x:any = []
    let fit_y:any = []
    //


    //I want my x range to be half in the fit line
    for (let n = 0; n<x.length; n++){
      if (n < x.length/2){
        fit_y.push(y[n])
        fit_x.push(x[n])
      }
    }


    const chiSqLossBg = (a: [number, number]) => {
      let result = 0;


      for (let i = 0; i < fit_x.length; i++) {

        const observed = fit_y[i]; //Experimental y values
        const expected = this.model(a, [fit_x[i]], [K_dat[i]])[0]; //model y outputs
        const diff = observed - expected;


        result += (diff * diff) / observed;
        //console.log(result)
      }

      //This will stay with the value from the last loop!
      this.chi_squared = (result).toFixed(4);
      return result
      

    };

    const parameters: [number, number] = fmin.nelderMead(chiSqLossBg, [...this.aParameters]);
    //console.log(parameters)
    return parameters
  }



  //finds the fit array based on the free parameters
  private calcModel(d_joined:any, parameters:any, r2_data:any){

    let x_val = d_joined.map(function(d:any) { return d.X; })
    let y_val = d_joined.map(function(d:any) { return d.Y; })
    
    //creating doubles since it's easier (personally for me)
    this.lambda =parseFloat( parameters.x[0]).toFixed(4);
    this.R =parameters.x[1]
    console.log(parameters, "prms")
    //Needed corrections, in the instructions
    this.displayed_R = this.R/2*0.197327
    //cut to 4 deimal points
    this.displayed_R = parseFloat(this.displayed_R).toFixed(4);
    let lambda = parameters.x[0]
    let R = parameters.x[1]

    let fit_x = []
    let fit_y = []


    //I want my x range to be half in the fit line
    for (let n = 0; n<x_val.length; n++){
      if (n < x_val.length/2){
        fit_y.push(y_val[n])
        fit_x.push(x_val[n])
      }
    }

    //so the condition stays true for the original selection
    let R_init = R

    //Add conditions later
    //find K here
    let K_dat = r2_data.map(function(d:any) { return d.K; })
    let X_dat = r2_data.map(function(d:any) { return d.X; })

    let y_fit = []



    ///I did this 2 time in 2 diff functions a it took less time to -
    // - copy paste once than to do a general function
    let c = 0
    while (c < y_val.length){
      if (y_val[c]== 0) {   ///Unitl where do I zoom
          y_val.splice(c, 1)
          x_val.splice(c, 1)
      }else{
         ++c
        }
      }

    let b = 0
    while (b < K_dat.length - x_val.length){
      if (b < K_dat.length - x_val.length) {   ///Unitl where do I zoom
          K_dat.splice(b, 1)
      }else{
         ++b
        }
      }


    //calculate Y for the fit based on x and free param   
    for (let s = 0; s < fit_x.length; s++){
      let yy = (1-lambda) + lambda*K_dat[s]*(1 + Math.exp(-fit_x[s]*fit_x[s] * R*R))
      y_fit.push(yy)
    }

    //make an array with X and Y values for the fit
    const fit_data = [];
    for (let s = 0; s < fit_x.length; s++) {
        fit_data.push([ fit_x[s], y_fit[s] ]);
    }

    //return the fit array for later use
    return fit_data

  }


  ///Adding a fit line
  private drawFit(d_joined:any[], x:any, y:any, fit_data:any): void {

    const line = d3.line();

    line.x((d) => x(d[0]))
    line.y((d) => y(d[1]))

    this.svg.append("path")
    .attr("d", line(fit_data))
    .attr("fill", "none")
    .attr("stroke", "red")
    .attr("stroke-width", 4)

    console.log(this.chi_squared, this.lambda, this.R)

  }


  public refreshNorm(min:any, max:any){
    console.log("The minimum range is "+min+ " and the maximum range is " + max); 
    this.enableR = true;// now you can choose R fit
    this.errDiv = [] //remove errorbars with each new plot

    if (parseFloat(min)>0 && parseFloat(max)<=0.4 && min != null && max != null){
      this.range_min = min;
      this.range_max = max;
      //delete and refraw plot
      this.svg.selectAll("*").remove();
      this.norm = true
      this.displayHist()
    }else{
      console.log("Prodive valid input");
    }

    
  }

  public noAccept = true; //So you can't add to the table before you actually fit!!
  //Have to add the let R line because of wierd error on taking value from selection
  public refreshFit(R_event:any){
    let R = (R_event.target as HTMLSelectElement).value;
    this.addFit = true;
    this.noAccept = false;
    this.R_choice = parseFloat(R)
    this.showParams = true;
    this.svg.selectAll("*").remove();
    this.displayHist()

    
  }

  public passVal(val:any){
    let v = (val.target as HTMLSelectElement).value;
    this.cent = v;
    this.enableDiv = true; //you can divide now
    this.enableNorm = false; //can't norm yet, only after u divide
    this.enableR = false;
    this.showDiv = false; // no divHist
    this.noAccept = true; //So you can't add to the table unless you FIT first!!
    this.showParams = false; //Stop displaying params when new dataset is loaded
    this.showTwo = true
    this.errDiv = [] //remove errorbars with each new plot
    this.svg.selectAll("*").remove();
  }

  constructor() { }


  public cc = 0;
  public initDisplay(){
    // So I only make SVG once
    if (this.cc == 0){
      this.createSvg();
      this.cc = this.cc+1;
      console.log("in to svg")
    }
    this.showDiv = true; // show div
    //Cancel normalization when I display new set of data!
    this.errDiv = [] //remove errorbars with each new plot
    this.enableNorm = true; // now you can normalize
    this.norm = false;
    this.addFit = false;
    this.showTwo = false // stop showing two hist
    this.displayHist();  
  }

  ngOnInit(): void {
   
  }


///HERE ADD THE DATA FOR THE TABLE

  public table_data: any[] = [];
  public headers = ["R", "Multiplicity"]
  public showTable = false;

  //Here is what happens when someone accepts the fit they made!
  public kk = 0
  public cent_added:any[] = []; //centralities added to the table

  public accept(){
    this.showTable = true;
    let rr = this.displayed_R

    //////Basically here I make sure that if you select a diffrent R for the same centrality, 
    // The R value in the table just gets replaced and not actually appending a new R with same Cent
    //Perhaps a bit more complicated than it should, but the only way I knew how!
    if (this.cent_added.includes(this.cent)){
      for (let g = 0; g<this.cent_added.length; g++){
        if (this.table_data[g]["Cent"] == this.cent){
          this.table_data[g]["R"] = rr
          break
        }
      }
    }else{
      this.cent_added.push(this.cent)
      if (this.cent == 0){
        this.table_data.push({"R": rr, "Multiplicity": "1601", "Cent":0});
      }else if (this.cent == 1){
        this.table_data.push({"R": rr, "Multiplicity": "1294", "Cent":1});
      }else if (this.cent == 2){
        this.table_data.push({"R": rr, "Multiplicity": "966", "Cent":2});
      }else if (this.cent == 3){
        this.table_data.push({"R": rr, "Multiplicity": "649", "Cent":3});
      }else if (this.cent == 4){
        this.table_data.push({"R": rr, "Multiplicity": "426", "Cent":4});
      }else if (this.cent == 5){
        this.table_data.push({"R": rr, "Multiplicity": "261", "Cent":5});
      }else if (this.cent == 6){
        this.table_data.push({"R": rr, "Multiplicity": "149", "Cent":6});
      }  
    }
    ///////////////  
              
  }



  private createScattSvg(): void {
    this.svg2 = d3.select("figure#scatter")
    .append("svg")
    .attr("width", this.width + (this.margin * 2))
    .attr("height", this.height + (this.margin * 2))
    .append("g")
    .attr("transform", "translate(" + this.margin + "," + this.margin + ")");
  }
  private drawPlot(): void {
  // Add X axis
  const x = d3.scaleLinear()
  .domain([0, 1700]) //hardcode
  .range([ 0, this.width ]);
  this.svg2.append("g")
  .style("font", "14px times")
  .attr("transform", "translate(0," + this.height + ")")
  .call(d3.axisBottom(x).tickFormat(d3.format("d")));

  // Add Y axis
  const y = d3.scaleLinear()
  .domain([2, 10])
  .range([ this.height, 0]);
  this.svg2.append("g")
  .call(d3.axisLeft(y))
  .style("font", "14px times");


  // Add dots
  const dots = this.svg2.append('g');
  dots.selectAll("dot")
  .data(this.table_data)
  .enter()
  .append("circle")
  .attr("cx", (d:any) => x(d.Multiplicity))
  .attr("cy", (d:any) => y(d.R))
  .attr("r", 7)
  .style("opacity", .8)
  .style("fill", "blue");

  // Add labels
  dots.selectAll("text")
  .data(this.table_data)
  .enter()
  .append("text")
  .text((d:any) => d.Framework)
  .attr("x", (d:any) => x(d.Multiplicity))
  .attr("y", (d:any) => y(d.R))

  this.svg2.append("text")
  .attr("class", "x label")
  .attr("text-anchor", "end")
  .attr("x", this.width)
  .attr("y", this.height + 50)
  .text("Multiplicity");

  this.svg2.append("text")
  .attr("class", "y label")
  .attr("text-anchor", "end")
  .attr("y", -50)
  .attr("dy", ".75em")
  .attr("transform", "rotate(-90)")
  .text("R");
}


  public scatterCount = 0;
  public finalPlot(){
    if (this.scatterCount == 0){
      this.createScattSvg()
      this.scatterCount = 1
    }
    this.showDiv = false;
    this.showScatter = true;
    this.svg2.selectAll("*").remove();
    this.drawPlot()
  }  

}